import React from 'react';

class NewsItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {title: this.props.title, body: this.props.body, id: this.props.id};
        this.getBody = this.getBody.bind(this);
    }

    getBody() {

        fetch('http://127.0.0.1:8000/api/get_news_body/' + this.state.id, {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(function (response) {
            return response.json();
        }).then(function(data) {
            console.log(data);
            this.setState({title: this.state.title, 'body': data.body});
            //
            // a.setState((prevState, data) => {
            //     return {title: prevState.title, body: data.body};
            // });

        }.bind(this));
    }

    render() {
        return (
            <div onClick={this.getBody}>
                <div className="title">{this.state.title}</div>
                <div className="body">{this.state.body}</div>
            </div>
        );
    }
}

export default NewsItem;
