import NewsList from './news_list.jsx';
import React from 'react';
import ReactDOM from 'react-dom';

//import ReactDOMServer from 'react-dom/server';
//var res = ReactDOMServer.renderToString(<HelloWorld />);

ReactDOM.render(<NewsList all_news={window.props.all_news}/>, document.getElementById('root'));
