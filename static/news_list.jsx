import React from 'react';
import NewsItem from './news_item.jsx';

class NewsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = this.props;
        console.log(this.props);
    }
    render() {
        return (
        <div>
            {
                this.state.all_news.map(function(news, i){
                    return <li key={i}>
                        <NewsItem title={news.title} body={news.body} id={i+1}/>
                    </li>
                })
            }
        </div>
        );
    }
}

export default NewsList;
