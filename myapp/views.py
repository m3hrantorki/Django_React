# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from react.render import render_component
from django.http import JsonResponse


def api_get_news_body(request, news_id=1):
    # fetch data from database
    return JsonResponse({'body': 'some text goes here ' + news_id})


def api_get_news_titles(request):
    # fetch data from database
    return {'titles': ['title 1', 'title 2', 'title 3']}


def home(request):
    titles = api_get_news_titles(None)

    component_markup = render_component('news_list.jsx',
                                        {'all_news':
                                             [
                                                 {'title': 'title1'},
                                                 {'title': 'title2'},
                                                 {'title': 'title3'}
                                             ]
                                        }
                                        )
    return render(request, 'home.html', {'rendered_component': component_markup})
